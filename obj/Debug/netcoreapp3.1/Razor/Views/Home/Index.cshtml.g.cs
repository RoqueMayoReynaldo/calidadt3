#pragma checksum "C:\Users\Alex\source\repos\ApplicationT3Calidad\Views\Home\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "a717a9b24efaf051484664e8c98f60b6532dda06"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Index), @"mvc.1.0.view", @"/Views/Home/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\Alex\source\repos\ApplicationT3Calidad\Views\_ViewImports.cshtml"
using ApplicationT3Calidad;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Alex\source\repos\ApplicationT3Calidad\Views\_ViewImports.cshtml"
using ApplicationT3Calidad.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"a717a9b24efaf051484664e8c98f60b6532dda06", @"/Views/Home/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"916a3f50d7bb81fea73fe9a2d453a36022dde781", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral(@"

<link rel=""stylesheet"" href=""https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css"">

<div id=""row"" class=""row "">


    <div class=""col-2 collapse"" id=""collapseExample"">
        <div class=""sticky-top"">
            <div class=""nav flex-column"">


            </div>
        </div>
    </div>


    <div class=""col-6"">


        <div class=""col-12 mt-3 mb-3"">
            <div class=""form-inline my-2 my-lg-0 "">

                <button class=""btn btn-info mr-3"" data-toggle=""collapse"" data-target=""#collapseExample"" aria-expanded=""false"" aria-controls=""collapseExample""><i class=""bi bi-list""></i></button>
                <input id=""buscador"" class=""form-control mr-sm-2"" type=""search"" style=""        width: 279px;"" placeholder=""Search"" aria-label=""Search"">
                <button class=""btn btn-primary"" id=""addNote""><i class=""bi bi-journal-plus""></i>Add Note</button>
            </div>

        </div>

        <div id=""notasLista"" class=""col-12"">

            <ul>

");
#nullable restore
#line 35 "C:\Users\Alex\source\repos\ApplicationT3Calidad\Views\Home\Index.cshtml"
                 foreach (Nota nota in Model)
                {


#line default
#line hidden
#nullable disable
            WriteLiteral("    <li class=\"liNota\"");
            BeginWriteAttribute("id", " id=", 1082, "", 1094, 1);
#nullable restore
#line 38 "C:\Users\Alex\source\repos\ApplicationT3Calidad\Views\Home\Index.cshtml"
WriteAttributeValue("", 1086, nota.id, 1086, 8, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\n\n        <div class=\"card w-75\">\n            <div class=\"card-body\">\n                <h5 class=\"card-title\">");
#nullable restore
#line 42 "C:\Users\Alex\source\repos\ApplicationT3Calidad\Views\Home\Index.cshtml"
                                  Write(nota.titulo);

#line default
#line hidden
#nullable disable
            WriteLiteral("</h5>\n                <p class=\"card-text\">");
#nullable restore
#line 43 "C:\Users\Alex\source\repos\ApplicationT3Calidad\Views\Home\Index.cshtml"
                                Write(nota.contenido.Substring(0, 50));

#line default
#line hidden
#nullable disable
            WriteLiteral("...</p>\n\n            </div>\n        </div>\n\n    </li>\n");
#nullable restore
#line 49 "C:\Users\Alex\source\repos\ApplicationT3Calidad\Views\Home\Index.cshtml"
    }

#line default
#line hidden
#nullable disable
            WriteLiteral("\n            </ul>\n\n\n        </div>\n\n\n    </div>\n\n\n    <div class=\"col-4\" id=\"colForm\">\n\n\n\n    </div>\n\n\n\n</div>\n\n\n");
            DefineSection("Scripts", async() => {
                WriteLiteral(@"


    <script>

        $(document).ready(


            function () {












                $(""#row"").on(""click"", ""#addNote"",


                    function (e) {


                        e.preventDefault();
                      

                        var req = $.ajax(

                            {
                                url: 'Home/formAdd',
                                type: 'get'

                            }

                        );

                        req.done(


                            function (respuesta) {

                                $(""#colForm"").html(respuesta);

                                var btnCrear = $(""#createNote"");
                                btnCrear.click(

                                    function (e) {

                                        e.preventDefault();


                                        var r = $.ajax(

                                            {

                                ");
                WriteLiteral(@"                url: 'Home/crearNota',
                                                type: 'post',
                                                data: {
                                                    titulo: $(""#inpTitulo"").val(),
                                                    contenido: $(""#inpContenido"").val()
                                                    

                                                }



                                            }

                                        );
                                        r.done(

                                            function (respuesta) {

                                              
                                                $(""#row"").html(respuesta);



                                            }




                                        );


                                    }







                                );


                            }









                   ");
                WriteLiteral(@"     );







                            


                    }


                );



                //getNota



                $(""#row"").on(""click"", "".liNota"",

                    function (e) {

                        e.preventDefault();
                        var liNota = $(this);

                        var request = $.ajax(
                            {

                                url: 'Home/getNota',
                                type: 'post',
                                data: {
                                    id: liNota.attr(""id"")
                                }



                            }


                        );
                        request.done(

                            function (res) {

                                $(""#colForm"").html(res);
                            }




                        );




                    }


                );

                //buscador



                $(""#row"").on(""keyup"", ""#buscador");
                WriteLiteral(@""",


                    function (e) {
                        e.preventDefault();


                        var buscador = $(""#buscador"");

                        var request = $.ajax(

                            {

                                url: 'Home/search',
                                type: 'post',
                                data: {
                                    buscar: buscador.val()
                                }




                            }



                        );
                        request.done(

                            function (respuesta) {

                                $(""#notasLista"").html(respuesta);
                            }


                        );



                    }






                );

                //--end



                //editarNota


                $(""#row"").on(""click"", ""#editar"",

                    function (e) {
                        e.preventDefault();

                        var request = $.ajax(
        ");
                WriteLiteral(@"                    {

                                url: 'Home/formEditar',
                                type: 'get',
                                data: {
                                    id: $(""#editar"").val()
                                }

                            }


                        );

                        request.done(

                            function (respuesta) {


                                $(""#colForm"").html(respuesta);

                                





                                var btnCrear = $(""#editarNote"");

                                btnCrear.click(

                                    function (e) {
                                        e.preventDefault();

                                        var r = $.ajax(

                                            {

                                                url: 'Home/guardarEditado',
                                                type: 'post',
                                           ");
                WriteLiteral(@"     data: {
                                                    titulo: $(""#inpTitulo"").val(),
                                                    contenido: $(""#inpContenido"").val(),
                                                    id: $(""#inputId"").val()
                                                    

                                                }



                                            }

                                        );

                                        r.done(

                                            function (respuesta) {

                                         
                                                $(""#row"").html(respuesta);



                                            }




                                        );


                                    }

                                );













                            }



                        );


                    }


                );

                //eliminar Nota


  ");
                WriteLiteral(@"              $(""#row"").on(""click"", ""#eliminarNota"",


                    function (e) {

                        e.preventDefault();

                        var request = $.ajax(
                            {

                                url: 'Home/eliminarNota',
                                type: 'get',
                                data: {
                                    id: $(""#eliminarNota"").val()
                                }

                            }


                        );

                        request.done(

                            function (respuesta) {

                            
                                $(""#row"").html(respuesta);
                            }



                        );




                    }




                );




            }





        );



                    


        



    </script>


");
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591

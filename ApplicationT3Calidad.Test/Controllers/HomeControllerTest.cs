﻿using ApplicationT3Calidad.Controllers;
using ApplicationT3Calidad.Models;
using ApplicationT3Calidad.Repository;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationT3Calidad.Test.ControllersTest
{
    public class HomeControllerTest
    {

        [Test]
        public void IndexCase01()
        {
            var mockNotaRepo = new Mock<INotaRepository>();

            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.Index();

            Assert.IsNotNull(resultado);

        }

        [Test]
        public void IndexCase02()
        {
            var mockNotaRepo = new Mock<INotaRepository>();

            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.Index();

            Assert.IsInstanceOf<ViewResult>(resultado);

        }

        [Test]
        public void IndexCase03()
        {
            var mockNotaRepo = new Mock<INotaRepository>();

            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.Index() as ViewResult;

            Assert.AreEqual("Index", resultado.ViewName);

        }

        [Test]
        public void IndexCase04()
        {
            var mockNotaRepo = new Mock<INotaRepository>();

            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.Index() as ViewResult;

            Assert.IsNull(resultado.TempData);

        }
        [Test]
        public void IndexCase05()
        {

            var mockNotaRepo = new Mock<INotaRepository>();
            mockNotaRepo.Setup(o => o.obtenerTodos()).Returns(new List<Nota>());


            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.Index() as ViewResult;

            Assert.IsNotNull(resultado.Model);

        }

        //formAddTest



        [Test]
        public void formAddCase01()
        {
            var mockNotaRepo = new Mock<INotaRepository>();

            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.formAdd();

            Assert.IsNotNull(resultado);

        }

        [Test]
        public void formAddCase02()
        {
            var mockNotaRepo = new Mock<INotaRepository>();

            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.formAdd();

            Assert.IsInstanceOf<ViewResult>(resultado);

        }

        [Test]
        public void formAddCase03()
        {
            var mockNotaRepo = new Mock<INotaRepository>();

            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.formAdd() as ViewResult;

            Assert.AreEqual("formAdd", resultado.ViewName);

        }

        [Test]
        public void formAddCase04()
        {
            var mockNotaRepo = new Mock<INotaRepository>();

            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.formAdd() as ViewResult;

            Assert.IsNull(resultado.TempData);

        }
        [Test]
        public void formAddCase05()
        {

            var mockNotaRepo = new Mock<INotaRepository>();
            mockNotaRepo.Setup(o => o.obtenerTodos()).Returns(new List<Nota>());


            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.formAdd() as ViewResult;

            Assert.IsNull(resultado.Model);

        }


        //crear nota cases

        [Test]
        public void crearNotaCase01()
        {
            var mockNotaRepo = new Mock<INotaRepository>();

            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.crearNota(new Nota());

            Assert.IsNotNull(resultado);

        }

        [Test]
        public void crearNotaCase02()
        {
            var mockNotaRepo = new Mock<INotaRepository>();

            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.crearNota(new Nota());

            Assert.IsInstanceOf<ViewResult>(resultado);

        }

        [Test]
        public void crearNotaCase03()
        {
            var mockNotaRepo = new Mock<INotaRepository>();

            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.crearNota(new Nota()) as ViewResult;

            Assert.AreEqual("Main", resultado.ViewName);

        }

        [Test]
        public void crearNotaCase04()
        {
            var mockNotaRepo = new Mock<INotaRepository>();

            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.crearNota(new Nota()) as ViewResult;

            Assert.IsNull(resultado.TempData);

        }
        [Test]
        public void crearNotaCase05()
        {

            var mockNotaRepo = new Mock<INotaRepository>();
            mockNotaRepo.Setup(o => o.obtenerTodos()).Returns(new List<Nota>());


            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.crearNota(new Nota()) as ViewResult;

            Assert.IsNotNull(resultado.Model);

        }


        //cases getNota


        [Test]
        public void getNotaCase01()
        {
            var mockNotaRepo = new Mock<INotaRepository>();
            mockNotaRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Nota());

            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.getNota(1);

            Assert.IsNotNull(resultado);

        }

        [Test]
        public void getNotaCase02()
        {
            var mockNotaRepo = new Mock<INotaRepository>();
            mockNotaRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Nota());

            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.getNota(2);

            Assert.IsInstanceOf<ViewResult>(resultado);

        }

        [Test]
        public void getNotaCase03()
        {
            var mockNotaRepo = new Mock<INotaRepository>();
            mockNotaRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Nota());
            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.getNota(3) as ViewResult;

            Assert.AreEqual("getNota", resultado.ViewName);

        }

        [Test]
        public void getNotaCase04()
        {
            var mockNotaRepo = new Mock<INotaRepository>();
            mockNotaRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Nota());
            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.getNota(5) as ViewResult;

            Assert.IsNull(resultado.TempData);

        }
        [Test]
        public void getNotaCase05()
        {

            var mockNotaRepo = new Mock<INotaRepository>();
            mockNotaRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Nota());


            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.getNota(11) as ViewResult;

            Assert.IsNotNull(resultado.Model);

        }


        //cases listaTodos


        [Test]
        public void listaTodosCase01()
        {
            var mockNotaRepo = new Mock<INotaRepository>();

            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.listaTodos();

            Assert.IsNotNull(resultado);

        }

        [Test]
        public void listaTodosCase02()
        {
            var mockNotaRepo = new Mock<INotaRepository>();

            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.listaTodos();

            Assert.IsInstanceOf<ViewResult>(resultado);

        }

        [Test]
        public void listaTodosCase03()
        {
            var mockNotaRepo = new Mock<INotaRepository>();

            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.listaTodos();

            Assert.AreEqual("listaTodos", resultado.ViewName);

        }

        [Test]
        public void listaTodosCase04()
        {
            var mockNotaRepo = new Mock<INotaRepository>();

            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.listaTodos();

            Assert.IsNull(resultado.TempData);

        }
        [Test]
        public void listaTodosCase05()
        {

            var mockNotaRepo = new Mock<INotaRepository>();
            mockNotaRepo.Setup(o => o.obtenerTodos()).Returns(new List<Nota>());


            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.listaTodos();

            Assert.IsNotNull(resultado.Model);

        }

        //cases search


        [Test]
        public void searchCase01()
        {
            var mockNotaRepo = new Mock<INotaRepository>();
            mockNotaRepo.Setup(o => o.obtenerTodos()).Returns(new List<Nota>());

            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.search(null);

            Assert.IsNotNull(resultado);

        }

        [Test]
        public void searchCase02()
        {
            var mockNotaRepo = new Mock<INotaRepository>();
            mockNotaRepo.Setup(o => o.obtenerTodos()).Returns(new List<Nota>());
            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.search(null);
            Assert.IsInstanceOf<ViewResult>(resultado);

        }

        [Test]
        public void searchCase03()
        {
            var mockNotaRepo = new Mock<INotaRepository>();
            mockNotaRepo.Setup(o => o.obtenerTodos()).Returns(new List<Nota>());
            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.search(null);

            Assert.AreEqual("listaTodos", resultado.ViewName);

        }

        [Test]
        public void searchCase04()
        {
            var mockNotaRepo = new Mock<INotaRepository>();
            mockNotaRepo.Setup(o => o.obtenerTodos()).Returns(new List<Nota>());
            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.search(null);

            Assert.IsNull(resultado.TempData);

        }
        [Test]
        public void searchCase05()
        {

            var mockNotaRepo = new Mock<INotaRepository>();
            mockNotaRepo.Setup(o => o.obtenerTodos()).Returns(new List<Nota>());


            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.search(null);

            Assert.IsNotNull(resultado.Model);

        }

        //

        [Test]
        public void searchCase06()
        {
            var mockNotaRepo = new Mock<INotaRepository>();
            mockNotaRepo.Setup(o => o.GetByTittleContent(It.IsAny<String>())).Returns(new List<Nota>());

            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.search("testeando");

            Assert.IsNotNull(resultado);

        }

        [Test]
        public void searchCase07()
        {
            var mockNotaRepo = new Mock<INotaRepository>();
            mockNotaRepo.Setup(o => o.GetByTittleContent(It.IsAny<String>())).Returns(new List<Nota>());

            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.search("testeando");
            Assert.IsInstanceOf<ViewResult>(resultado);

        }

        [Test]
        public void searchCase08()
        {
            var mockNotaRepo = new Mock<INotaRepository>();
            mockNotaRepo.Setup(o => o.GetByTittleContent(It.IsAny<String>())).Returns(new List<Nota>());

            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.search("testeando");

            Assert.AreEqual("listaTodos", resultado.ViewName);

        }

        [Test]
        public void searchCase09()
        {
            var mockNotaRepo = new Mock<INotaRepository>();
            mockNotaRepo.Setup(o => o.GetByTittleContent(It.IsAny<String>())).Returns(new List<Nota>());

            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.search("testeando");

            Assert.IsNull(resultado.TempData);

        }
        [Test]
        public void searchCase10()
        {

            var mockNotaRepo = new Mock<INotaRepository>();
            mockNotaRepo.Setup(o => o.GetByTittleContent(It.IsAny<String>())).Returns(new List<Nota>());

            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.search("testeando");

            Assert.IsNotNull(resultado.Model);

        }


        //cases formEditar



        [Test]
        public void formEditarCase01()
        {
            var mockNotaRepo = new Mock<INotaRepository>();
            mockNotaRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Nota());
            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.formEditar(1);

            Assert.IsNotNull(resultado);

        }

        [Test]
        public void formEditarCase02()
        {
            var mockNotaRepo = new Mock<INotaRepository>();
            mockNotaRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Nota());
            HomeController hc = new HomeController(null, mockNotaRepo.Object);
            var resultado = hc.formEditar(2);

            Assert.IsInstanceOf<ViewResult>(resultado);

        }

        [Test]
        public void formEditarCase03()
        {
            var mockNotaRepo = new Mock<INotaRepository>();
            mockNotaRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Nota());
            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.formEditar(3) as ViewResult;

            Assert.AreEqual("formEditar", resultado.ViewName);

        }

        [Test]
        public void formEditarCase04()
        {
            var mockNotaRepo = new Mock<INotaRepository>();
            mockNotaRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Nota());
            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.formEditar(4) as ViewResult;

            Assert.IsNull(resultado.TempData);

        }
        [Test]
        public void formEditarCase05()
        {

            var mockNotaRepo = new Mock<INotaRepository>();
            mockNotaRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Nota());


            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.formEditar(5) as ViewResult;

            Assert.IsNotNull(resultado.Model);

        }


        //cases guardarEditado



        [Test]
        public void guardarEditadoCase01()
        {
            var mockNotaRepo = new Mock<INotaRepository>();
            mockNotaRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Nota());
            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.guardarEditado(new Nota());

            Assert.IsNotNull(resultado);

        }

        [Test]
        public void guardarEditadoCase02()
        {
            var mockNotaRepo = new Mock<INotaRepository>();
            mockNotaRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Nota());
            HomeController hc = new HomeController(null, mockNotaRepo.Object);
            var resultado = hc.guardarEditado(new Nota());

            Assert.IsInstanceOf<ViewResult>(resultado);

        }

        [Test]
        public void guardarEditadoCase03()
        {
            var mockNotaRepo = new Mock<INotaRepository>();
            mockNotaRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Nota());
            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.guardarEditado(new Nota()) as ViewResult;

            Assert.AreEqual("Main", resultado.ViewName);

        }

        [Test]
        public void guardarEditadoCase04()
        {
            var mockNotaRepo = new Mock<INotaRepository>();
            mockNotaRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Nota());
            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.guardarEditado(new Nota()) as ViewResult;

            Assert.IsNull(resultado.TempData);

        }
        [Test]
        public void guardarEditadoCase05()
        {

            var mockNotaRepo = new Mock<INotaRepository>();
            mockNotaRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Nota());
            mockNotaRepo.Setup(o => o.obtenerTodos()).Returns(new List<Nota>());

            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.guardarEditado(new Nota()) as ViewResult;

            Assert.IsNotNull(resultado.Model);

        }

        //cases eliminarNota



        [Test]
        public void DeleteNotaCase01()
        {
            var mockNotaRepo = new Mock<INotaRepository>();
            mockNotaRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Nota());
            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.eliminarNota(1);

            Assert.IsNotNull(resultado);

        }

        [Test]
        public void DeleteNotaCase02()
        {
            var mockNotaRepo = new Mock<INotaRepository>();
            mockNotaRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Nota());
            HomeController hc = new HomeController(null, mockNotaRepo.Object);
            var resultado = hc.eliminarNota(2);

            Assert.IsInstanceOf<ViewResult>(resultado);

        }

        [Test]
        public void DeleteNotaCase03()
        {
            var mockNotaRepo = new Mock<INotaRepository>();
            mockNotaRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Nota());
            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.eliminarNota(3) as ViewResult;

            Assert.AreEqual("Main", resultado.ViewName);

        }

        [Test]
        public void DeleteNotaCase04()
        {
            var mockNotaRepo = new Mock<INotaRepository>();
            mockNotaRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Nota());
            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.eliminarNota(4) as ViewResult;

            Assert.IsNull(resultado.TempData);

        }
        [Test]
        public void DeleteNotaCase05()
        {

            var mockNotaRepo = new Mock<INotaRepository>();
            mockNotaRepo.Setup(o => o.FindById(It.IsAny<int>())).Returns(new Nota());
            mockNotaRepo.Setup(o => o.obtenerTodos()).Returns(new List<Nota>());

            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.eliminarNota(5) as ViewResult;

            Assert.IsNotNull(resultado.Model);

        }

        //cases Main


        [Test]
        public void MainCase01()
        {
            var mockNotaRepo = new Mock<INotaRepository>();
        
            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.Main();

            Assert.IsNotNull(resultado);

        }

        [Test]
        public void MainCase02()
        {
            var mockNotaRepo = new Mock<INotaRepository>();
       
            HomeController hc = new HomeController(null, mockNotaRepo.Object);
            var resultado = hc.Main();

            Assert.IsInstanceOf<ViewResult>(resultado);

        }

        [Test]
        public void MainCase03()
        {
            var mockNotaRepo = new Mock<INotaRepository>();
           
            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.Main() as ViewResult;

            Assert.AreEqual("Main", resultado.ViewName);

        }

        [Test]
        public void MainCase04()
        {
            var mockNotaRepo = new Mock<INotaRepository>();
        
            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.Main() as ViewResult;

            Assert.IsNull(resultado.TempData);

        }
        [Test]
        public void MainCase05()
        {

            var mockNotaRepo = new Mock<INotaRepository>();
         
            mockNotaRepo.Setup(o => o.obtenerTodos()).Returns(new List<Nota>());

            HomeController hc = new HomeController(null, mockNotaRepo.Object);

            var resultado = hc.Main() as ViewResult;

            Assert.IsNotNull(resultado.Model);

        }

    }
}

﻿using ApplicationT3Calidad.DB;
using ApplicationT3Calidad.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationT3Calidad.Repository
{
    public interface INotaRepository
    {

        List<Nota> obtenerTodos();
        void AddNota(Nota nota);

        void DeleteNota(Nota nota);

        void UpdateNota( );

        Nota FindById(int id);

        List<Nota> GetByTittleContent(String buscar);
    }


    public class NotaRepository : INotaRepository
    {
        private readonly ApplicationT3Context context;

        public NotaRepository(ApplicationT3Context context)
        {
            this.context = context;
        }

        public void AddNota(Nota nota)
        {
            context.Notas.Add(nota);

            context.SaveChanges();
        }

        public void DeleteNota(Nota nota)
        {
            context.Notas.Remove(nota);
            context.SaveChanges();
        }

        public Nota FindById(int id)
        {
            return context.Notas.FirstOrDefault(o => o.id == id);
        }

        public List<Nota> GetByTittleContent(string buscar)
        {
            return context.Notas.Where(o => o.titulo.Contains(buscar) || o.contenido.Contains(buscar)).ToList();
        }

        public List<Nota> obtenerTodos()
        {
            return context.Notas.ToList();
        }

        public void UpdateNota()
        {
            context.SaveChanges();
        }
    }
}

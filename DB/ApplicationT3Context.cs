﻿using ApplicationT3Calidad.DB.Mapping;
using ApplicationT3Calidad.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationT3Calidad.DB
{
    public class ApplicationT3Context : DbContext
    {
        public DbSet<Nota> Notas { get; set; }
     
        public ApplicationT3Context(DbContextOptions<ApplicationT3Context> options) : base(options) { }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new NotaMap());
        
        }

    }
}

﻿using ApplicationT3Calidad.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationT3Calidad.DB.Mapping
{
    public class NotaMap : IEntityTypeConfiguration<Nota>

    {
        public void Configure(EntityTypeBuilder<Nota> builder)
        {


            builder.ToTable("Nota", "dbo");
            builder.HasKey(Nota => Nota.id);


        }
    }

}

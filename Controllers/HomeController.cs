﻿using ApplicationT3Calidad.DB;
using ApplicationT3Calidad.Models;
using ApplicationT3Calidad.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationT3Calidad.Controllers
{

    


    public class HomeController : Controller
    {

       



        private readonly ILogger<HomeController> _logger;
        private readonly INotaRepository context;

        public HomeController(ILogger<HomeController> logger, INotaRepository context)
        {
            _logger = logger;
            this.context = context;
        }

     

        public IActionResult Index()
        {
            return View("Index",context.obtenerTodos());
        }

        public ViewResult formAdd()
        {


            return View("formAdd");
        }

        [HttpPost]
        public ViewResult crearNota(Nota nota)
        {
            nota.fechaModificacion = DateTime.Now;

            context.AddNota(nota);


            return View("Main", context.obtenerTodos());
        }

        [HttpPost]
        public ViewResult getNota(int id)
        {
            Nota nota = context.FindById(id);
            return View("getNota",nota);

        }

        [HttpPost]
        public ViewResult listaTodos()
        {


            List<Nota> notas = context.obtenerTodos();



            return View("listaTodos",notas);

        }


        [HttpPost]
        public ViewResult search(string buscar)
        {
            if (string.IsNullOrEmpty(buscar))
            {
                List<Nota> n = context.obtenerTodos();
                return View("listaTodos", n);
            }

            //filtramos por titulo,contenido 
            List<Nota> nota = context.GetByTittleContent(buscar);



            return View("listaTodos", nota);

        }


        [HttpGet]
        public ViewResult formEditar(int id)
        {
            Nota nota = context.FindById(id);


            return View("formEditar",nota);

        }

        [HttpPost]
        public ViewResult guardarEditado(Nota nota)
        {




            Nota note = context.FindById(nota.id);

            note.fechaModificacion = DateTime.Now;
            note.titulo = nota.titulo;
            note.contenido = nota.contenido;

            context.UpdateNota();

            return View("Main", context.obtenerTodos());

        }




        [HttpGet]
        public ViewResult eliminarNota(int id)
        {


            Nota nota = context.FindById(id);


            context.DeleteNota(nota);

            return View("Main", context.obtenerTodos());
        }

        public ViewResult Main()
        {
           



            return View("Main", context.obtenerTodos());
        }


    }
}
